
package com.example.smd;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class weather extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather);
        TextView message=findViewById(R.id.temperature);
        TextView desc=findViewById(R.id.desc);
        TextView citytag=findViewById(R.id.cityy);
        TextView preciptag=findViewById(R.id.preciptag);
        TextView windsp=findViewById(R.id.windspeed);
        TextView winddir=findViewById(R.id.winddir);
        TextView humid=findViewById(R.id.humidity);
        TextView coun=findViewById(R.id.country);



        Intent intent=getIntent();
        String city_name=intent.getStringExtra("city");

        String url="http://api.weatherstack.com/current?access_key=3b58907d7fde4d4ba3b60698f77ae2c5&query="+city_name;



        JsonObjectRequest req=new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
             try {
                 JSONObject main_obj=response.getJSONObject("current");
                 JSONObject get_city=response.getJSONObject("location");
                 String loc=get_city.getString("country");
                 String ws=main_obj.getString("wind_speed");
                 String wdir=main_obj.getString("wind_dir");
                 String hum=main_obj.getString("humidity");
                 String get_precip=main_obj.getString("precip");
                 String dees=main_obj.getString("weather_descriptions");
                 String city=get_city.getString("name");
                 String temp=main_obj.getString("temperature");


                 coun.append(loc);
                 windsp.append(ws);
                 winddir.append(wdir);
                 humid.append(hum);
                 preciptag.append(get_precip);
                 desc.append(dees);
                 citytag.append(city);
                 message.append(temp);


            }catch(JSONException e){
                 Toast.makeText(weather. this,e.getMessage(),Toast.LENGTH_LONG).show();
            }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError err){
                Toast.makeText(weather. this,err.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue=Volley.newRequestQueue(this);
        queue.add(req);

    }
}