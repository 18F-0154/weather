package com.example.smd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText city=findViewById(R.id.edittext);
        Button submit=findViewById(R.id.button);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String string=city.getText().toString();
                Intent intent=new Intent(MainActivity.this,weather.class);
                intent.putExtra("city",string);
                startActivity(intent);


            }



        });
    }
}

